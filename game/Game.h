#ifndef GAME_GAME_H_
#define GAME_GAME_H_

//C system includes

//C++ system includes
#include <cstdint>

//Third-party includes

//Own includes
#include "game/config/GameCfg.h"
#include "manager_utils/drawing/Image.h"
#include "manager_utils/drawing/Text.h"

//Forward Declarations
struct InputEvent;

class Game {
public:
	int32_t init(const GameCfg &cfg);
	void deinit();
	void draw();
	void handleEvent(const InputEvent &event);

private:
	void setMousePosText(const Point &mousePos);

	Text _mousePosText;
};

#endif /* GAME_GAME_H_ */
