#ifndef GAME_CONFIG_GAMECFG_H_
#define GAME_CONFIG_GAMECFG_H_

//C system includes

//C++ system includes
#include <cstdint>

//Third-party includes

//Own includes

//Forward Declarations

struct GameCfg {
	int32_t pressKeysRsrcId;

	int32_t textFontId;
};

#endif /* GAME_CONFIG_GAMECFG_H_ */
