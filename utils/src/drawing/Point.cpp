//Corresponding Header
#include "utils/drawing/Point.h"

//C system includes

//C++ system includes

//Third-party includes

//Own includes

const Point Point::ZERO(0, 0);
const Point Point::UNDEFINED(100000, 100000);

Point::Point(int32_t inputX, int32_t inputY) :
		x(inputX), y(inputY) {

}

bool Point::operator==(const Point &other) const {
	return (x == other.x) && (x == other.y);
}

bool Point::operator!=(const Point &other) const {
	return !operator==(other);
}
