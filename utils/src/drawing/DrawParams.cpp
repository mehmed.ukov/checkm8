//Corresponding Header
#include "utils/drawing/DrawParams.h"

//C system includes

//C++ system includes

//Third-party includes

//Own includes

void DrawParams::reset() {
	pos = Point::UNDEFINED;

	width = 0;
	height = 0;

	opacity = FULL_OPACITY;

	rsrcId = INVALID_RESOURCE_ID;

	widgetType = WidgetType::UNKNOWN;
}
