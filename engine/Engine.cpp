//Corresponding Header
#include "engine/Engine.h"

//C system includes

//C++ system includes
#include <iostream>

//Third-party includes

//Own includes
#include "engine/config/EngineConfig.h"
#include "manager_utils/managers/DrawMgr.h"

#include "utils/thread/ThreadUtils.h"
#include "utils/drawing/DrawParams.h"
#include "utils/time/Time.h"

#include "sdl_utils/Texture.h"

int32_t Engine::init(const EngineConfig &cfg) {
	if (EXIT_SUCCESS != _managerHandler.init(cfg.managerHandlerCfg)) {
		std::cerr << "_managerHandler.init() failed." << std::endl;
		return EXIT_FAILURE;
	}

	if (EXIT_SUCCESS != _event.init()) {
		std::cerr << "InputEvent.init() failed." << std::endl;
		return EXIT_FAILURE;
	}

	if (EXIT_SUCCESS != _game.init(cfg.gameCfg)) {
		std::cerr << "_game.init() failed." << std::endl;
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

void Engine::deinit() {
	_game.deinit();
	_event.deinit();
	_managerHandler.deinit();
}

void Engine::start() {
	mainLoop();
}

void Engine::mainLoop() {
	Time time;

	while (true) {
		time.getElapsed();

		const bool shouldExit = processFrame();
		if (shouldExit) {
			break;
		}

		limitFPS(time.getElapsed().toMicroseconds());
	}
}

void Engine::drawFrame() {
	gDrawMgr->clearScreen();

	_game.draw();

	gDrawMgr->finishFrame();
}

bool Engine::processFrame() {
	while (_event.pollEvent()) {
		if (_event.checkForExitRequest()) {
			return true;
		}
		handleEvent();
	}
	drawFrame();

	return false;
}

void Engine::handleEvent() {
	_game.handleEvent(_event);
}

void Engine::limitFPS(int64_t elapsedTimeMicroSeconds) {
	constexpr auto maxFrames = 60;
	constexpr auto microSecondsInASecond = 1000000;
	constexpr auto microSecondsPerFrame = microSecondsInASecond / maxFrames;
	const int64_t sleepDurationMicorSeconds = microSecondsPerFrame
			- elapsedTimeMicroSeconds;

	if (sleepDurationMicorSeconds > 0) {
		Threading::sleepFor(sleepDurationMicorSeconds);
	}
}

