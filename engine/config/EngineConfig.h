#ifndef ENGINE_CONFIG_ENGINECONFIG_H_
#define ENGINE_CONFIG_ENGINECONFIG_H_

//C system includes

//C++ system includes
#include <cstdint>

//Third-party includes

//Own includes
#include "manager_utils/config/ManagerHandlerCfg.h"
#include "game/config/GameCfg.h"

//Forward Declarations

struct EngineConfig {
	ManagerHandlerCfg managerHandlerCfg;
	GameCfg gameCfg;
};

#endif /* ENGINE_CONFIG_ENGINECONFIG_H_ */
