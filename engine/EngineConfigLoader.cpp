//Corresponding Header
#include "EngineConfigLoader.h"

//C system includes

//C++ system includes

//Third-party includes

//Own includes
#include "common/CommonDefines.h"

namespace {
constexpr auto WINDOW_WIDTH = 800;
constexpr auto WINDOW_HEIGHT = 600;
constexpr auto WINDOW_NAME = "Texts_Colors";

constexpr auto PRESS_KEYS_WIDTH = 640;
constexpr auto PRESS_KEYS_HEIGHT = 480;

constexpr auto LAYER_2_IMG_WIDTH = 150;
constexpr auto LAYER_2_IMG_HEIGHT = 150;

constexpr auto ANGELINE_VINTAGE_40_FONT_SIZE = 40;

constexpr auto MAX_FRAME_RATE = 60;
}

static std::string getFilePath(const std::string relativePath) {
#ifdef NDEBUG
	return relativePath;
#else
	return "../" + relativePath;
#endif
}

static void populateTextContainerConfig(TextContainerCfg &cfg) {
	FontCfg fontCfg;
	fontCfg.location = getFilePath("resources/fonts/AngelineVintage.ttf");
	fontCfg.fontSize = ANGELINE_VINTAGE_40_FONT_SIZE;
	cfg.fontConfigs.insert(std::make_pair(Fonts::ANGELINE_VINTAGE_BIG, fontCfg));
}

static void populateImageContainerConfig(ImageContainerCfg &cfg) {
	ImageCfg imageCfg;
	imageCfg.location = getFilePath("resources/pictures/press_keys.png");
	imageCfg.width = PRESS_KEYS_WIDTH;
	imageCfg.height = PRESS_KEYS_HEIGHT;
	cfg.imageConfigs.insert(std::make_pair(Textures::PRESS_KEYS, imageCfg));
}

static void populateMonitorConfig(MonitorWindowCfg &cfg) {
	cfg.windowName = WINDOW_NAME;
	cfg.windowWidth = WINDOW_WIDTH;
	cfg.windowHeight = WINDOW_HEIGHT;
	cfg.windowFlags = WINDOW_SHOWN;
}

static void populateRsrcMgrConfig(RsrcMgrCfg &cfg) {
	populateImageContainerConfig(cfg.imageContainerCfg);
	populateTextContainerConfig(cfg.textContainerCfg);
}

static void populateDrawMgrConfig(DrawMgrCfg &cfg) {
	populateMonitorConfig(cfg.windowCfg);
	cfg.maxFrameRate = MAX_FRAME_RATE;
}

static void populateManagerHandlerConfig(ManagerHandlerCfg &cfg) {
	populateDrawMgrConfig(cfg.drawMgrCfg);
	populateRsrcMgrConfig(cfg.rsrcMgrCfg);
}

static void populateGameConfig(GameCfg &cfg) {
	cfg.pressKeysRsrcId = Textures::PRESS_KEYS;

	cfg.textFontId = Fonts::ANGELINE_VINTAGE_BIG;
}

EngineConfig EngineConfigLoader::loadConfig() {
	EngineConfig cfg;

	populateManagerHandlerConfig(cfg.managerHandlerCfg);
	populateGameConfig(cfg.gameCfg);

	return cfg;
}

