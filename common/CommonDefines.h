#ifndef COMMON_COMMONDEFINES_H_
#define COMMON_COMMONDEFINES_H_

//C system includes

//C++ system includes
#include <cstdint>

//Third-party includes

//Own includes

//Forward Declarations

namespace Textures {
enum Images {
	PRESS_KEYS
};
} //namespace Textures

namespace Fonts {
enum FontId {
	ANGELINE_VINTAGE_SMALL, ANGELINE_VINTAGE_BIG
};
} //namespace Fonts

#endif /* COMMON_COMMONDEFINES_H_ */
