#ifndef SDL_UTILS_RENDERER_H_
#define SDL_UTILS_RENDERER_H_

//C system includes

//C++ system includes
#include <cstdint>

//Third-party includes

//Own includes
#include "utils/drawing/DrawParams.h"

//Forward declarations
struct SDL_Renderer;
struct SDL_Texture;
struct SDL_Window;

class Renderer {
public:
	Renderer() = default;

	Renderer(const Renderer &other) = delete;
	Renderer(Renderer &&other) = delete;

	Renderer& operator=(const Renderer &other) = delete;
	Renderer& operator=(Renderer &&other) = delete;

	int32_t init(SDL_Window *window);
	void deinit();
	void clearScreen();
	void finishFrame();
	void renderTexture(SDL_Texture *texture, const DrawParams &drawParams);
	void setWidgetBlendMode(SDL_Texture *texture, BlendMode blendMode);
	void setWidgetOpacity(SDL_Texture *texture, int32_t opacity);

private:
	void drawImage(SDL_Texture *texture, const DrawParams &drawParams);
	void drawText(SDL_Texture *texture, const DrawParams &drawParams);

	SDL_Renderer *_sdlRenderer = nullptr;
};

#endif /* SDL_UTILS_RENDERER_H_ */
