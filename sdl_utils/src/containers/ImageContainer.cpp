//Corresponding Header
#include "sdl_utils/containers/ImageContainer.h"

//C system includes

//C++ system includes
#include <iostream>

//Third-party includes

//Own includes
#include "sdl_utils/Texture.h"

int32_t ImageContainer::init(const ImageContainerCfg &cfg) {
	for (const auto &pair : cfg.imageConfigs) {
		const auto element = pair.second;
		const auto rsrcId = pair.first;

		if (EXIT_SUCCESS != loadSingleResource(element, rsrcId)) {
			std::cerr << "loadSingleResource() failed for file: " << element.location
					<< std::endl;
			return EXIT_FAILURE;
		}
	}

	return EXIT_SUCCESS;
}

void ImageContainer::deinit() {
	for (auto &pair : _textures) {
		Texture::freeTexture(pair.second);
	}
}

SDL_Texture* ImageContainer::getImageTexture(int32_t rsrcId) const {
	auto it = _textures.find(rsrcId);
	if (it == _textures.end()) {
		std::cerr << "Error invalid rsrcId: " << rsrcId << " requested!"
				<< std::endl;
		return nullptr;
	}

	return it->second;
}

Rectangle ImageContainer::getImageFrame(int32_t rsrcId) const {
	auto it = _textureFrames.find(rsrcId);
	if (it == _textureFrames.end()) {
		std::cerr << "Error invalid rsrcId: " << rsrcId
				<< " requested! Returning ZERO Rectangle!" << std::endl;
		return Rectangle::ZERO;
	}

	return it->second;
}

int32_t ImageContainer::loadSingleResource(const ImageCfg &resourcesCfg,
		int32_t rsrcId) {

	SDL_Texture *texture = nullptr;
	if (EXIT_SUCCESS
			!= Texture::createTextureFromFile(resourcesCfg.location, texture)) {
		std::cerr << "Texture::createSurfaceFromFile() failed for file: "
				<< resourcesCfg.location << std::endl;
		return EXIT_FAILURE;
	}

	_textures[rsrcId] = texture;

	Rectangle &rect = _textureFrames[rsrcId];

	rect.x = 0;
	rect.y = 0;
	rect.w = resourcesCfg.width;
	rect.h = resourcesCfg.height;

	return EXIT_SUCCESS;
}
