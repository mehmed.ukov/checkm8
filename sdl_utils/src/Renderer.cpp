//Corresponding Header
#include "sdl_utils/Renderer.h"

//C system includes

//C++ system includes
#include <iostream>

//Third-party includes
#include <SDL_render.h>
#include <SDL_hints.h>

//Own includes
#include <sdl_utils/Texture.h>

int32_t Renderer::init(SDL_Window *window) {

	if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1")) {
		std::cerr << "Warning: Linear texture filtering not enabled! "
				"SDL_SetHint() failed. SDL Error: " << SDL_GetError() << std::endl;
		return EXIT_FAILURE;
	}

	constexpr auto unspecifiedDriverId = -1;
	_sdlRenderer = SDL_CreateRenderer(window, unspecifiedDriverId,
			SDL_RENDERER_ACCELERATED);

	if (_sdlRenderer == nullptr) {
		std::cerr << "SDL_CreateRenderer() failed. Reason: " << SDL_GetError()
				<< std::endl;
		return EXIT_FAILURE;
	}

	if (SDL_SetRenderDrawColor(_sdlRenderer, 0, 0, 255, 255) != EXIT_SUCCESS) {
		std::cerr << "SDL_SetRenderDrawColor() failed. Reason: " << SDL_GetError()
				<< std::endl;
		return EXIT_FAILURE;
	}

	Texture::setRenderer(_sdlRenderer);

	return EXIT_SUCCESS;
}

void Renderer::deinit() {
	if (_sdlRenderer) {
		SDL_DestroyRenderer(_sdlRenderer);
		_sdlRenderer = nullptr;
	}
}

void Renderer::clearScreen() {
	if (SDL_RenderClear(_sdlRenderer) != EXIT_SUCCESS) {
		std::cerr << "SDL_SetRenderDrawColor() failed. Reason: " << SDL_GetError()
				<< std::endl;
	}
}

void Renderer::finishFrame() {
	SDL_RenderPresent(_sdlRenderer);
}

void Renderer::renderTexture(SDL_Texture *texture,
		const DrawParams &drawParams) {
	if (WidgetType::IMAGE == drawParams.widgetType) {
		drawImage(texture, drawParams);
	} else if (WidgetType::TEXT == drawParams.widgetType) {
		drawText(texture, drawParams);
	} else {
		std::cerr << "Error, received unsupported WidgetType: "
				<< static_cast<int32_t>(drawParams.widgetType) << " for rsrcId: "
				<< drawParams.rsrcId << std::endl;
	}
}

void Renderer::setWidgetBlendMode(SDL_Texture *texture, BlendMode blendMode) {
	if (EXIT_SUCCESS != Texture::setBlendModeTexture(texture, blendMode)) {
		std::cerr << "Texture::setBlendModeTexture() failed" << std::endl;
	}
}

void Renderer::setWidgetOpacity(SDL_Texture *texture, int32_t opacity) {
	if (EXIT_SUCCESS != Texture::setAlphaTexture(texture, opacity)) {
		std::cerr << "Texture::setAlphaTexture() failed" << std::endl;
	}
}

void Renderer::drawImage(SDL_Texture *texture, const DrawParams &drawParams) {
	const SDL_Rect destRect = { .x = drawParams.pos.x, .y = drawParams.pos.y, .w =
			drawParams.width, .h = drawParams.height };

	int32_t err = EXIT_SUCCESS;
	if (FULL_OPACITY == drawParams.opacity) {
		err = SDL_RenderCopy(_sdlRenderer, texture, nullptr, &destRect);
	} else {
		if (EXIT_SUCCESS != Texture::setAlphaTexture(texture, drawParams.opacity)) {
			std::cerr << "Texture::setAlphaTexture() failed for rsrcId: "
					<< drawParams.rsrcId << std::endl;
		}
		err = SDL_RenderCopy(_sdlRenderer, texture, nullptr, &destRect);

		if (EXIT_SUCCESS != Texture::setAlphaTexture(texture, FULL_OPACITY)) {
			std::cerr << "Texture::setAlphaTexture() failed for rsrcId: "
					<< drawParams.rsrcId << std::endl;
		}
	}

	if (EXIT_SUCCESS != err) {
		std::cerr << "SDL_RenderCopy() failed for rsrcId: " << drawParams.rsrcId
				<< " Reason: " << SDL_GetError() << std::endl;
	}
}

void Renderer::drawText(SDL_Texture *texture, const DrawParams &drawParams) {
	const SDL_Rect destRect = { .x = drawParams.pos.x, .y = drawParams.pos.y, .w =
			drawParams.width, .h = drawParams.height };

	const int32_t err = SDL_RenderCopy(_sdlRenderer, texture, nullptr, &destRect);

	if (EXIT_SUCCESS != err) {
		std::cerr << "SDL_RenderCopy() failed for rsrcId: " << drawParams.rsrcId
				<< " Reason: " << SDL_GetError() << std::endl;
	}
}

