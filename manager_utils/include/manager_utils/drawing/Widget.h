#ifndef MANAGER_UTILS_INCLUDE_MANAGER_UTILS_DRAWING_WIDGET_H_
#define MANAGER_UTILS_INCLUDE_MANAGER_UTILS_DRAWING_WIDGET_H_

//C system includes

//C++ system includes
#include <cstdint>

//Third-party includes

//Own includes
#include "utils/drawing/DrawParams.h"

//Forward Declarations

class Widget {
public:
	void draw();

	void reset();

	void setWidth(int32_t width);
	void setHeight(int32_t height);

	int32_t getWidth() const;
	int32_t getHeight() const;

	void setPosition(const Point &position);
	void setPosition(int32_t x, int32_t y);
	void setOpacity(int32_t opacity);

	int32_t getOpacity() const;
	Point getPosition() const;

	void activateAlphaModulation();
	void deactivateAlphaModulation();

	void show();
	void hide();

	void moveRight(int32_t delta);
	void moveLeft(int32_t delta);
	void moveUp(int32_t delta);
	void moveDown(int32_t delta);

protected:
	DrawParams _drawParams;

	bool _isCreated = false;
	bool _isDestroyed = true;
	bool _isVisible = true;
	bool _isAlphaModulationEnabled = false;
};

#endif /* MANAGER_UTILS_INCLUDE_MANAGER_UTILS_DRAWING_WIDGET_H_ */
